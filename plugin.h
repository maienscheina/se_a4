/** 
 * File:   plugin.h
 * Author: Lehmann
 *
 * Inface definition for smart sensors.
 * 
 * Created on 10. November 2013, 15:49
 */

#ifndef PLUGIN_H
#define	PLUGIN_H

#include "FestoProcessAccess.h"

enum plugin_states{FirstQuarter, SecondQuarter, ThirdQuarter, FourthQuarter, NoItem};
enum profile_values{Low, Mid, High};
enum profile_error_codes{Profil_OK, Falsche_Stelle, Teil_Fehlt};

class Plugin {
private:
	plugin_states currentstate;
	FestoProcessAccess *process;
	bool result;
	int height;
	static unsigned char profile[4];
	bool heightChangeDetected(int height);
public:
	void evalCycle();
	bool getResult();
	void resetResult();
	int getProfile();
	Plugin(FestoProcessAccess *process);
	~Plugin();
};


#endif	/* PLUGIN_H */
