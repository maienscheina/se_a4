/*
 * test.h
 *
 *  Created on: 01.06.2017
 *      Author: acd409
 */

#ifndef TEST_H_
#define TEST_H_

namespace Plugin {

class test {
	private:
	void testReady_Standby();
	FestoProcessAccess *process;
	test();
};

} /* namespace Plugin */

#endif /* TEST_H_ */
