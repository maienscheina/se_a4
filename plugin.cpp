/*
 * Plugin.cpp
 *
 *  Created on: 01.06.2017
 *      Author: acd409
 */

#include "plugin.h"


Plugin::Plugin(FestoProcessAccess *process){
	this->process = process;
	this->result = false;
	this->currentstate = NoItem;
	this->height = 0;
	this->profile[0] = 0;
	this->profile[1] = 0;
	this->profile[2] = 0;
	this->profile[3] = 0;
}


Plugin::~Plugin(){
	delete process;
}


void Plugin::evalCycle(){

	height = process->getHight();

	switch(currentstate) {
	case NoItem:
		if (heightChangeDetected(height)){
			currentstate = FirstQuarter;

			if(height > 3300 && height < 3500){
				profile[FirstQuarter] = Mid;
			}
			else if(height < 3200){
				profile[FirstQuarter] = High;
			}
			else if(height > 3500 && height < 3760){
				profile[FirstQuarter] = Low;
			}
		}
		break;

	case FirstQuarter:
		if (heightChangeDetected(height)){
			currentstate = SecondQuarter;

			if(height > 3300 && height < 3500){
				profile[SecondQuarter] = Mid;
			}
			else if(height < 3200){
				profile[SecondQuarter] = High;
			}
			else if(height > 3500 && height < 3760){
				profile[SecondQuarter] = Low;
			}
		}
		break;
	case SecondQuarter:
		if (heightChangeDetected(height)){
			currentstate = ThirdQuarter;

			if(height > 3300 && height < 3500){
				profile[ThirdQuarter] = Mid;
			}
			else if(height < 3200){
				profile[ThirdQuarter] = High;
			}
			else if(height > 3500 && height < 3760){
				profile[ThirdQuarter] = Low;
			}
		}
		break;

	case ThirdQuarter:
		if (heightChangeDetected(height)){
			currentstate = FourthQuarter;

			if(height > 3300 && height < 3500){
				profile[FourthQuarter] = Mid;
			}
			else if(height < 3200){
				profile[FourthQuarter] = High;
			}
			else if(height > 3500 && height < 3760){
				profile[FourthQuarter] = Low;
			}
		}
		break;
	case FourthQuarter:
		if (heightChangeDetected(height)){
			currentstate = NoItem;
			result = true;
		}
		break;
	}
}

bool Plugin::heightChangeDetected(int height){
	static int height_old = 0;
	if(height > (height_old + 100) || height < (height_old - 100) ){
		height_old = height;
		return true;
	}
	else height_old = height;
	return false;
}

bool Plugin::getResult(){
	return result;
}

void Plugin::resetResult(){
	result = false;
}

int Plugin::getProfile(){
	if((profile[FirstQuarter] == Mid) && (profile[SecondQuarter] == High) && (profile[ThirdQuarter] == Low) && (profile[FourthQuarter] == Mid)){
		return Profil_OK;
	}
	else if ((profile[FirstQuarter] == High) && (profile[SecondQuarter] == Mid) && (profile[ThirdQuarter] == Low) && (profile[FourthQuarter] == Mid)){
		return Falsche_Stelle;
	}
	else if ((profile[FirstQuarter] == Mid) && (profile[SecondQuarter] == Mid) && (profile[ThirdQuarter] == Low) && (profile[FourthQuarter] == Mid)){
		return Teil_Fehlt;
	}
}

